package com.company;

import java.util.ArrayList;

public class Usuario {
    private String nome;
    private int idade;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getIdade(int i) {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public ArrayList<Integer> jogar(Dado dado) {
        return dado.jogarDados();
    }
}

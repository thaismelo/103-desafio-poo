package com.company;

import java.util.Scanner;

public class IU {
    private Scanner leitura = new Scanner(System.in);

    public void init() {
        Usuario usuario = new Usuario();
        System.out.println("Olá, vamos jogar dados?");
        System.out.println("Qual o seu nome?");
        usuario.setNome(leitura.next());
        System.out.println(usuario.getNome() + ", qual a sua idade?");
        usuario.getIdade(leitura.nextInt());
        System.out.println("Se deseja jogar dados tradicional, digite 1. Caso queira costumizavel, digite 2:");

        boolean isCostumizavel = (leitura.nextInt() == 1 ? false : true);
        Dado dado = new Dado();

        if (isCostumizavel) {
            System.out.println("Qual a quantidade de faces que deseja?");
            dado = new Dado(leitura.nextInt());
        }

        System.out.println("...... SORTEANDO DADOS COM " + dado.getQuantidadeFaces() + " FACES ......");
        System.out.println("Os números sorteados foram: " + usuario.jogar(dado));

    }

}

package com.company;
import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.Random;

public class Dado {
    private Random random = new Random();
    private ArrayList<Integer> faces = new ArrayList<>();
    private ArrayList<Integer> numerosSorteados = new ArrayList<>();

    public int quantidadeFaces = 6;

    public Dado() {

    }

    public Dado(int quantidadeFaces) {
        this.quantidadeFaces = quantidadeFaces;
        this.verificarQuantidadeFaces();
    }

    public int getQuantidadeFaces() {
        return quantidadeFaces;
    }

    public ArrayList<Integer> jogarDados() {
        this.sortearTresVezes();
        return numerosSorteados;
    }

    private void verificarQuantidadeFaces() {
        for (int i = 1; i <= quantidadeFaces; i++) {
            faces.add(i);
        }
    }

    private int sortearUmNumero() {
        System.out.println("faces: " + faces.size());
        return random.nextInt(faces.size());
    }

    private void sortearDadosTresVezes() {
        int somaNumeroSorteados = 0;

        for (int j = 1; j <= 3; j++) {
            int numeroSorteado = this.sortearUmNumero();
            System.out.println(numeroSorteado);
            somaNumeroSorteados += numeroSorteado;
            numerosSorteados.add(numeroSorteado);
        }
        numerosSorteados.add(somaNumeroSorteados);
        System.out.println(numerosSorteados);
    }

    private void sortearTresVezes() {
        for (int e = 1; e <= 1; e++) {
            this.sortearDadosTresVezes();
        }

    }
}
